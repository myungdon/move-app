import React, {useState} from "react";

const Page4 = () => {
    const [pay, setPay] = useState(0)
    const [player1, setPlayer1] = useState('')
    const [player2, setPlayer2] = useState('')
    const [player3, setPlayer3] = useState('')

    const [pay1, setPay1] = useState(0)
    const [pay2, setPay2] = useState(0)
    const [pay3, setPay3] = useState(0)

    const [bestPlayer, setBest] = useState('')

    const getPay = e => {
        setPay(e.target.value)
    }

    const getP1 = e => {
        setPlayer1(e.target.value)
    }

    const getP2 = e => {
        setPlayer2(e.target.value)
    }

    const getP3 = e => {
        setPlayer3(e.target.value)
    }
    const randomPay = () => {
        let p1 = Math.round((Math.random() * pay + 1 ) / 100) * 100
        let p2 = Math.round((Math.random() * (pay - p1 + 1)) / 100) * 100
        let p3 = pay - p1 - p2

        setPay1(p1)
        setPay2(p2)
        setPay3(p3)

        if(p1 > p2 && p1 > p3) setBest(player1)
        else if (p2 > p3) setBest(player2)
        else setBest(player3)
    }

    return (
        <div>
            <h1>복불복 엔빵</h1>
            <p>금액: <input type="text" value={pay} onChange={getPay}/> 원</p>
            <br/>
            <input className="inputBox" type="text" value={player1} onChange={getP1}/>
            <input className="inputBox" type="text" value={player2} onChange={getP2}/>
            <input className="inputBox" type="text" value={player3} onChange={getP3}/>
            <br/>
            <button onClick={randomPay}>시작</button>
            <p>{player1} : {pay1}원</p>
            <p>{player2} : {pay2}원</p>
            <p>{player3} : {pay3}원</p>
            <p className="bestPlayer">{bestPlayer}님 축하 합니다</p>
        </div>
    )
}

export default Page4