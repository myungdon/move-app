import React, {useState} from "react";

const Page2 = () => {
    const [sang, setSang] = useState('')
    const [ha, setHa] = useState('')
    const [a, setA] = useState('')
    const [b, setB] = useState('')

    const handleSang = e => {
        setSang(e.target.value)
    }

    const handleHa = e => {
        setHa(e.target.value)
    }

    const pickCodi = () => {
        const topArray = sang.split(',')
        const bottomArray = ha.split(',')
        setA(topArray[Math.floor(Math.random() * topArray.length)])
        setB(bottomArray[Math.floor(Math.random() * bottomArray.length)])
    }

    return (
        <div>
            <h1>내일의 코디</h1>
            상의: <input type="text" value={sang} onChange={handleSang}/>
            <br/>
            하의: <input type="text" value={ha} onChange={handleHa}/>
            <br/>
            <button onClick={pickCodi}>내일 뭐 입징?</button>
            <br/>
            <p>상의: {a}</p>
            <p>하의: {b}</p>
            <p>입으세요</p>
        </div>
    )
}

export default Page2