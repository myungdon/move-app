import React, {useState} from "react";

const Page1 = () => {
    const [a, setA] = useState('')
    const [b, setB] = useState('')
    const [percent, setPercent] = useState(0)

    const handleText1 = e => {
        setA(e.target.value)
    }

    const handleText2 = e => {
        setB(e.target.value)
    }

    const handlePercentChange = () => {
        setPercent(Math.floor(Math.random() * 101))
    }

    return (
        <div>
            <h1>궁합도 보기</h1>
                <input type="text" value={a} onChange={handleText1}/>
                <input type="text" value={b} onChange={handleText2}/>
                <button onClick={handlePercentChange}>화긴</button>
            <div>{a}랑(이랑) {b}의 궁합도는 {percent} % 입니다</div>
        </div>
    )
}

export default Page1