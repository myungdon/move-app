import React, {useState} from "react";

const Page3 = () => {
    const INIT_PRICE = 10000
    const BOX_PRICE = 2000

    const [cash, setCash] = useState(INIT_PRICE)
    const [pick, setPick] = useState([])

    const pokemons = [
        { name: '피카츄', percent: 0.2},
        { name: '라이츄', percent: 0.1},
        { name: '파이리', percent: 0.1},
        { name: '꼬부기', percent: 0.1},
        { name: '버터플', percent: 0.1},
        { name: '야도란', percent: 0.1},
        { name: '피죤투', percent: 0.1},
        { name: '또가스', percent: 0.1},
        { name: '뮤', percent: 0.05},
        { name: '뮤츠', percent: 0.05},
    ]

    // 확률 구할 때 많이 쓰는 로직
    const getRandomPokemons = () => {
        const rand = Math.random()
        let sum = 0
        for (const pokemon of pokemons) {
            sum += pokemon.percent
            if (rand < sum) return pokemon
        }
    }

    const pickPokemon = () => {
        if (cash < BOX_PRICE) return false // 돈 없으면 못 뽑게 함
        const pokemon = getRandomPokemons() // 확률로 포켓몬 뽑아옴
        setCash(cash - BOX_PRICE) // 현금 차감
        setPick([...pick, pokemon.name]) // 뽑아온 포켓몬 배열에 넣음
    }

    const resetGame = () => {
        setCash(INIT_PRICE)
        setPick([])
    }

    return (
        <div>
            <h1>포켓몬 띠부띠부씰</h1>
            <p>보유 현금: {cash}</p>
            <button onClick={pickPokemon}>갓챠</button>
            <h2>당첨 기록</h2>
            <ul>
                {pick.map((item, index) => (
                    <li key={index}>{item}</li>
                ))}
            </ul>
            <button onClick={resetGame}>다시하기</button>
        </div>
    )
}

export default Page3