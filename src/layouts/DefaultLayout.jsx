import React from "react";
import {Link} from "react-router-dom";

const DefaultLayout = ({children}) => {
    return (
        <>
            <div>
                헤더
                <nav>
                    <Link to={"/"}>1 페이지</Link>
                    |
                    <Link to={"/2"}>2 페이지</Link>
                    |
                    <Link to={"/3"}>3 페이지</Link>
                    |
                    <Link to={"/4"}>4 페이지</Link>
                </nav>
            </div>
            <main>{children}</main>
            <footer>푸터</footer>
        </>
    )
}

export default DefaultLayout